<?php

namespace Tests\Unit;
use Tests\TestCase;

class QuestionTest extends TestCase
{
    /**
     * A basic unit test to fetch translated question list.
     *
     * @return void
     */
    public function testGetQuestionList()
    {
        $data = ['lang' => 'fr'];
        $response = $this->get(route('question_list',$data));
        $response->assertStatus(200)
            ->assertJsonStructure(['status', 'data', 'message']); // Returns 200 with Json Array if successfully get question list
        $data = ['lang1' => 'fr'];
        $response = $this->get(route('question_list',$data));
        $response->assertStatus(422); // Returns 422 if validation fails
    }

    /**
     * A basic unit test to save question data to current source
     *
     * @return void
     */
    public function testStoreQuestionData()
    {
        $data =  [
            "text" => "what is the capital of India", 
            "createdAt" => "2020-06-02 00:00:00", 
            "choices" => [
                [
                    "text" => "Chennai" 
                ], 
                [
                    "text" => "New Delhi" 
                ], 
                [
                    "text" => "Agra" 
                ] 
            ] 
        ];
        $response = $this->post(route('question_save'), $data);
        $response->assertStatus(200)
            ->assertJsonStructure(['status', 'data', 'message']); // Returns 200 with Json Array if successfully saved question data
        $response = $this->post(route('question_save'), []);
        $response->assertStatus(422); // Returns 422 if validation fails
        $data =  [
            "text1" => "what is the capital of India", 
            "createdAt1" => "2020-06-02 00:00:00", 
            "choices" => [
                [
                    "text" => "Chennai" 
                ], 
                [
                    "text" => "New Delhi" 
                ], 
                [
                    "text" => "Agra" 
                ],
                [
                    "text" => "Mumbai" 
                ] 
            ] 
        ];
        $response = $this->post(route('question_save'), $data);
        $response->assertStatus(422)
            ->assertJsonStructure(['status', 'data', 'message']);; // Returns 422 if validation fails
    }


}
