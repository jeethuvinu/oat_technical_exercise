1. Clone the project source code and Install all the dependencies files using composer:
``````````````````````````````````````````````````````````````````````````````````````
    composer install

2. Additional Packages Installed: 
````````````````````````````````
    2.1 For Google Translate API library - stichoza/google-translate-php
    2.2 For Code Quality Check - nunomaduro/phpinsights --dev

3. Set up Environment file
````````````````````````
    Can configure .env and other data source related configuration variables by copying the content from .env.example file

4. Start the local development server
````````````````````````````````````
    php artisan serve

5. End Point URL with sample input
``````````````````````````````````

    1. Get All Questions after translating to a specific language : 
        http://127.0.0.1:8000/api/questions?lang=en

    2. Post a Question with its choices: 
        http://127.0.0.1:8000/api/questions 
        
        Request Payload: 

        {
            "text": "what is the capital of India",
            "createdAt": "2021-09-30 00:00:00",
            "choices": [
                {
                    "text": "Chennai"
                },
                {
                    "text": "New Delhi"
                },
                {
                    "text": "Agra"
                }
            ]
        }

6. For Unit Test
````````````````
    php artisan test

7. Cache 
````````  
    Currently we are storing the translated question details in cache file driver. As per requirements in future, we can store in redis, memcached db
