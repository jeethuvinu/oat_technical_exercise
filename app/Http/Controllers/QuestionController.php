<?php

namespace App\Http\Controllers;

use App\Http\Requests\GetQuestionRequest;
use App\Http\Requests\StoreQuestionRequest;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class QuestionController extends Controller
{
    protected $questionSource;
    public function __construct()
    {
        $this->questionSource = app()->make('QuestionDataSourceService');
    }

    /**
     * List all translated questions and associated choices
     *
     * @param GetQuestionRequest $request
     *
     * @return JsonArray
     */
    public function getQuestions(GetQuestionRequest $request)
    {
        try {
            $data = $request->validated();
            $getData = $this->questionSource->getTranslatedQuestionList($data['lang']);
            return response()->json($getData, 200);
        } catch (\Exception $error) {
            return $error->getMessage();
        }
    }

    /**
     * Store questions and associated choices to current data source
     *
     * @param StoreQuestionRequest $request
     *
     * @return JsonArray
     */
    public function storeQuestion(StoreQuestionRequest $request)
    {
        try {
            $data = $request->validated();
            $saveData = $this->questionSource->saveQuestionData($data);
            return response()->json($saveData, 200);
        } catch (\Exception $error) {
            return $error->getMessage();
        }
    }
}
