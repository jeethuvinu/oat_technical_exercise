<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;

class CsvQuestionService
{
    /**
     * Method to get Question data from CSV file
     *
     * @return Array
     */
    public function getQuestionCsvData()
    {
        if (! Storage::exists(env('SOURCE_CSV'))) {
            return [];
        }
        $filename = Storage::disk('local')->path(env('SOURCE_CSV'));
        return $this->csvToArray($filename);
    }

    // Method to convert CSV file to an array
    public function csvToArray($filename = '', $delimiter = ',')
    {
        if (! file_exists($filename) || ! is_readable($filename)) { // If file exists or not
            return false;
        }
        $data = [];
        // If file exists, reading the csv and save as array
        $handle = fopen($filename, 'r');
        if ($handle !== false) {
            $data = $this->fetchCsvData($handle, $delimiter); // Method to loop and fetch the csv data
            fclose($handle);
        }
        return $data;
    }

    // Method to get contents of a CSV file
    public function fetchCsvData($handle, $delimiter)
    {
        $header = null;
        $data = $choiceArray = $questionArray = [];
        while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
            if (! $header) {
                $header = ['text', 'createdAt', 'choices'];
            } else {
                $choiceArray = [];
                $choices = array_slice($row, count($header) - 1);
                $choiceCount = count($choices);
                for ($i = 0; $i < $choiceCount; $i++) {
                    $dataItem = ['text' => $choices[$i]];
                    array_push($choiceArray, $dataItem);
                }
                $questionArray = array_slice($row, 0, count($header) - 1);
                array_push($questionArray, $choiceArray);
                array_push($data, array_combine($header, $questionArray));
            }
        }
        return $data;
    }

    /**
     * Store questions and associated choices to csv source file
     *
     * @param Array $payLoad
     *
     * @return Array
     */
    public function saveToQuestionCSV($payLoad)
    {
        $choiceArray = [];
        if (! Storage::exists(env('SOURCE_CSV'))) {
            return ['status' => false, 'data' => [], 'message' => 'source file not exist'];
        }
        $filename = Storage::disk('local')->path(env('SOURCE_CSV')); // Getting the file from the path
        $file_open = fopen($filename, 'a');
        foreach ($payLoad['choices'] as $choice) {
            array_push($choiceArray, $choice['text']);
        }
        $questionArray = array_slice($payLoad, 0, 2);
        $finalArray = array_merge($questionArray, $choiceArray);
        fputcsv($file_open, $finalArray);
        return ['status' => true, 'data' => $payLoad, 'message' => 'success'];
    }
}
