<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;

class JsonQuestionService
{
    /**
     * Method to get Question data from Json file
     *
     * @return Array
     */
    public function getQuestionJsonData()
    {
        if (! Storage::exists(env('SOURCE_JSON'))) {
            return [];
        }
        $filename = Storage::disk('local')->path(env('SOURCE_JSON'));
        $str = file_get_contents($filename);
        return json_decode($str, true);
    }

    /**
     * Store questions and associated choices to json source file
     *
     * @param Array $payLoad
     *
     * @return Array
     */
    public function saveToQuestionJson($payLoad)
    {
        if (! Storage::exists(env('SOURCE_JSON'))) {
            return ['status' => false, 'data' => [], 'message' => 'source file not exist'];
        }
        $filename = Storage::disk('local')->path(env('SOURCE_JSON')); // Getting the file from the path
        $fileInput = file_get_contents($filename);
        $tempArray = json_decode($fileInput);
        array_push($tempArray, $payLoad);
        $jsonData = json_encode($tempArray);
        file_put_contents($filename, $jsonData);
        return ['status' => true, 'data' => $payLoad, 'message' => 'success'];
    }
}
