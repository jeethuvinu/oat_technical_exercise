<?php

namespace App\Services;

use Illuminate\Support\Facades\Cache;
use Stichoza\GoogleTranslate\GoogleTranslate;

class TranslateQuestionService
{
    /**
     * Method to translate the Question list and save or fetch from cache
     *
     * @param array $data, string $lang
     *
     * @return array
     */
    public function getTranslatedData(array $data, string $lang)
    {
        try {
            $translate = new GoogleTranslate();
            $translate->setSource('en');
            $translate->setTarget($lang);
            $translatedArray = [];
            foreach ($data as $item) {
                $text = '';
                if (Cache::has($lang.':'.$translate->translate($item['text']))) {
                    $text = Cache::get($lang.':'.$translate->translate($item['text']));
                } else {
                    $text = $translate->translate($item['text']);
                    Cache::put($lang.':'.$translate->translate($item['text']), $text);
                }
                $item = ['text' => $text,'choices' => $this->translateChoices($item['choices'], $translate, $lang)];
                array_push($translatedArray, $item);
            }
            return ['status' => true, 'data' => $translatedArray, 'message' => 'success'];
        } catch (\Exception $error) {
            return ['status' => false, 'data' => [], 'message' => $error->getMessage()];
        }
    }

    /**
     * Translate choices of each questions
     *
     * @param array $choices, object $translate, string $lang
     *
     * @return Array
     */
    public function translateChoices($choices, $translate, $lang)
    {
        $choiceArray = [];
        foreach ($choices as $choice) {
            $text = '';
            if (Cache::has($lang.':'.$translate->translate($choice['text']))) {
                $text = Cache::get($lang.':'.$translate->translate($choice['text']));
            } else {
                $text = $translate->translate($choice['text']);
                Cache::put($lang.':'.$translate->translate($choice['text']), $text);
            }
            array_push($choiceArray, $translate->translate($text));
        }
        return $choiceArray;
    }
}
