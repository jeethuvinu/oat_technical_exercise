<?php

namespace App\Services;

class QuestionDataSourceService
{
    protected $source;
    protected $csvDataService;
    protected $jsonDataService;
    protected $translateService;

    public function __construct()
    {
        $this->source = env('SOURCE');
        $this->csvDataService = app()->make('CsvQuestionService');
        $this->jsonDataService = app()->make('JsonQuestionService');
        $this->translateService = app()->make('TranslateQuestionService');
    }

    /**
     * Get Questions and choices list from current source and translate it into specific language
     *
     * @param String $lang
     *
     * @return JsonArray
     */
    public function getTranslatedQuestionList($lang)
    {
        $data = [];
        switch ($this->source) {
            case 'json':
                $data = $this->jsonDataService->getQuestionJsonData(); // Function to fetch data from json
                break;
            case 'csv':
                $data = $this->csvDataService->getQuestionCsvData(); // Function to fetch data from csv
                break;
            case 'mysql': // Can implement it in future
                break;
            default:
                return ['status' => false, 'message' => 'Invalid Source'];
        }
        if (! $data) {
            return ['status' => false, 'data' => [], 'message' => 'source file not exist'];
        }
        return $this->translateService->getTranslatedData($data, $lang); // Function to translate the result question list
    }

    /**
     * Store questions and associated choices to current data source
     *
     * @param Array $payLoad
     *
     * @return Array
     */
    public function saveQuestionData($payLoad = [])
    {
        switch ($this->source) {
            case 'json':
                $data = $this->jsonDataService->saveToQuestionJson($payLoad); // Function call to save the the data as Json
                break;
            case 'csv':
                $data = $this->csvDataService->saveToQuestionCSV($payLoad); // Function call to save the the data as Csv
                break;
            case 'mysql': // Can implement it in future
                break;
            default:
                return ['status' => false, 'message' => 'Invalid Source'];
        }
        return $data;
    }
}
