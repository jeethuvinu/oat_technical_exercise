<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('QuestionDataSourceService', \App\Services\QuestionDataSourceService::class);
        $this->app->bind('JsonQuestionService', \App\Services\JsonQuestionService::class);
        $this->app->bind('CsvQuestionService', \App\Services\CsvQuestionService::class);
        $this->app->bind('TranslateQuestionService', \App\Services\TranslateQuestionService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
